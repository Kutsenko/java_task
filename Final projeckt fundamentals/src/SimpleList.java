/**
 * Created by Egor on 21.03.2017.
 */
public interface SimpleList {
    void add (String s);
    String get ();
    String get (int id);
    String remove () throws Exception;
    String remove(int id);
    Boolean delete();
}
