/**
 * Created by Egor on 21.03.2017.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        DynamicStringList myList = new DynamicStringList(2);
        myList.add("First String");
        myList.add("Second String");
        myList.add("Third String");
        myList.add("Fourth String");
        myList.add("Fifth String");
        myList.add("Sixth String");
        myList.add("Seventh String");
        System.out.println("get "+myList.get());
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("get by index 2 "+ myList.get(2));
        System.out.println(myList.toString());
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("remove " + myList.remove());
        System.out.println(myList.toString());
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("remove by index 2 " + myList.remove(2));
        System.out.println(myList.toString());
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("delete " +myList.delete());
        System.out.println(myList.toString());
    }
}
