/**
 * Created by Egor on 21.03.2017.
 */
public class DynamicStringList implements SimpleList {
    private String array[];
    private static final int capacity = 10;
    private int size = 0;

    public DynamicStringList() {
        this.array = new String[capacity];
    }

    public DynamicStringList(int capacity) {
        this.array = new String[capacity];
    }

    @Override
    public void add(String s) {
        if (size == array.length) {
            String[] newArray = new String[(int) (array.length * 1.5)];
            System.arraycopy(array, 0, newArray, 0, array.length);
            this.array = newArray;
        }
        array[size] = s;
        size++;
    }

    @Override
    public String get() {
        return array[size - 1];
    }

    @Override
    public String get(int id) {
        if (id < 0 || size - 1 < id) {
            throw new IndexOutOfBoundsException();
        }
        return array[id];
    }

    @Override
    public String remove() throws Exception {
        if (size < 1) {
            throw new Exception("Can not remove item from empty list.");
        }
        String lastElement = array[size - 1];
        array[size - 1] = null;
        size--;
        return lastElement;
    }

    @Override
    public String remove(int id) {
        if (id < 0 || size - 1 < id) {
            throw new IndexOutOfBoundsException();
        }
        String element = array[id];
        System.arraycopy(array, id + 1, array, id, size - 1 - id);
        size--;
        return element;
    }

    @Override
    public Boolean delete() {
        if (size == 0) {
            return false;
        }
        size = 0;
        array = new String[capacity];
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("DynamicStringList{");
        for (int i = 0; i < size; i++) {
            if (i < size - 1) {
                sb.append(array[i]).append(", ");
            } else {
                sb.append(array[i]);
            }
        }
        sb.append("}");
        return sb.toString();
    }
}

